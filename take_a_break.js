const openSite = require('open')
let userInput = process.argv[2]

const timeTillDingInMinutes = userInput
const timeTillDingInSeconds = timeTillDingInMinutes * 60
const pluralMinute = timeTillDingInMinutes == 1 ? '' : 's'
const pluralSecond = timeTillDingInSeconds == 1 ? '' : 's'
if (timeTillDingInMinutes < 1) {
console.log(`${timeTillDingInSeconds} second${pluralSecond}`)
}
else{
    console.log(`${timeTillDingInMinutes} minute${pluralMinute}`)
}
console.log(`The website I will open is ${process.argv[3]}`)
const getTimeInSeconds = () => {
    const d = new Date();
    const n = d.getTime();
    return n / 1000
}


(async function () {    
    const startTime = getTimeInSeconds()
    const endTime = startTime + timeTillDingInSeconds
    let currentTime = startTime;
    while (currentTime <= endTime+5) {
        currentTime = getTimeInSeconds()
        if (currentTime >= endTime) {
            await openSite(`https://${process.argv[3]}`)
            break
        }
    }
})()